import {ListPage} from '../pageObjects/list.po';
import {DetailPage} from '../pageObjects/detail.po';
import {NewPage} from '../pageObjects/new.po';
import {browser, logging} from 'protractor';

describe('Tests', () => {
  let listPage: ListPage;
  let newPage: NewPage;
  let detailPage: DetailPage;

  beforeEach(() => {
    listPage = new ListPage();
    listPage.navigateTo();
  });

  it('navigate to list should display list title', () => {
    expect(listPage.getTitleText()).toEqual('Overzicht van alle items');
  });

  it('navigate to list should display 3 items', () => {
    expect(listPage.getAmountOfItems()).toEqual(3);
    expect(listPage.getItemText(1)).toEqual('Keuken schoonmaken');
    expect(listPage.getItemText(2)).toEqual('Boodschappen doen');
    expect(listPage.getItemText(3)).toEqual('Afspraak kapper');
  });

  it('navigate to new should display new title', () => {
    listPage.clickNew();
    newPage = new NewPage();
    expect(newPage.getTitleText()).toEqual('Nieuw item');
  });

  it('navigate to new and abort making new item', () => {
    expect(listPage.getAmountOfItems()).toEqual(3);
    listPage.clickNew();
    newPage = new NewPage();
    newPage.sendKeysItemTitle('Nieuw item titel');
    newPage.sendKeysItemText('Nieuw item tekst');
    newPage.clickList();
    listPage = new ListPage();
    expect(listPage.getAmountOfItems()).toEqual(3);
  });

  it('navigate to detail should display detail title', () => {
    listPage.clickItem(1);
    detailPage = new DetailPage();
    expect(detailPage.getTitleText()).toEqual('Details van een item');
  });

  it('navigate to detail should display item title and text', () => {
    listPage.clickItem(1);
    detailPage = new DetailPage();
    expect(detailPage.getItemTitleText()).toEqual('Keuken schoonmaken');
    expect(detailPage.getItemTextText()).toEqual('De keuken moet nog worden schoongemaakt');
  });

  it('navigate to detail and back to list', () => {
    expect(listPage.getAmountOfItems()).toEqual(3);
    listPage.clickItem(1);
    detailPage = new DetailPage();
    detailPage.clickList();
    listPage = new ListPage();
    expect(listPage.getAmountOfItems()).toEqual(3);
  });

  it('navigate to new, make new item, navigate to detail and remove new item', () => {
    expect(listPage.getAmountOfItems()).toEqual(3);
    listPage.clickNew();
    newPage = new NewPage();
    newPage.sendKeysItemTitle('Nieuw item titel');
    newPage.sendKeysItemText('Nieuw item tekst');
    newPage.clickSave();
    listPage = new ListPage();
    expect(listPage.getAmountOfItems()).toEqual(4);
    expect(listPage.getItemText(4)).toEqual('Nieuw item titel');
    listPage.clickItem(4);
    detailPage = new DetailPage();
    detailPage.clickRemove();
    listPage = new ListPage();
    expect(listPage.getAmountOfItems()).toEqual(3);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
