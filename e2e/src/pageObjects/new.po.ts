import { by, element } from 'protractor';

export class NewPage {

  getTitleText(): Promise<string> {
    return element(by.css('#title')).getText() as Promise<string>;
  }

  sendKeysItemTitle(title: string): void {
    element(by.css('#item_title_input')).sendKeys(title);
  }

  sendKeysItemText(text: string): void {
    element(by.css('#item_text_input')).sendKeys(text);
  }

  clickSave(): Promise<unknown> {
    return element(by.css('#btn_save')).click() as Promise<unknown>;
  }

  clickList(): void {
    element(by.css('#btn_list')).click();
  }
}
