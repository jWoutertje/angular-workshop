import { browser, by, element } from 'protractor';

export class ListPage {
  navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl) as Promise<unknown>;
  }

  getTitleText(): Promise<string> {
    return element(by.css('#title')).getText() as Promise<string>;
  }

  getItemText(id: number): Promise<string> {
    return element(by.css('#item_title_' + id)).getText() as Promise<string>;
  }

  getAmountOfItems(): Promise<number> {
    return element.all(by.css('mat-list-item')).count() as Promise<number>;
  }

  clickItem(id: number): Promise<unknown> {
    return element(by.css('#item_title_' + id)).click() as Promise<unknown>;
  }

  clickNew(): Promise<unknown> {
    return element(by.css('#btn_new')).click() as Promise<unknown>;
  }
}
