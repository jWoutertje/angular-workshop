import { by, element } from 'protractor';

export class DetailPage {

  getTitleText(): Promise<string> {
    return element(by.css('#title')).getText() as Promise<string>;
  }

  getItemTitleText(): Promise<string> {
    return element(by.css('#item_title')).getText() as Promise<string>;
  }

  getItemTextText(): Promise<string> {
    return element(by.css('#item_text')).getText() as Promise<string>;
  }

  clickRemove(): Promise<unknown> {
    return element(by.css('#btn_remove')).click() as Promise<unknown>;
  }

  clickList(): void {
    element(by.css('#btn_list')).click();
  }
}
