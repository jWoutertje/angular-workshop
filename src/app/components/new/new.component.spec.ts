import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewComponent} from './new.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {ItemService} from '../../services/item.service';

describe('NewComponent', () => {
  let component: NewComponent;
  let fixture: ComponentFixture<NewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [NewComponent],
      providers: [ItemService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show titel input', () => {
    const titel = fixture.nativeElement.querySelector('#item_title_input').tagName;
    expect(titel).toBe('INPUT');
  });

  it('should show tekst input', () => {
    const tekst = fixture.nativeElement.querySelector('#item_text_input').tagName;
    expect(tekst).toBe('TEXTAREA');
  });

  it('should show save button', () => {
    const save = fixture.nativeElement.querySelector('#btn_save').textContent;
    expect(save).toBe('Opslaan');
  });

  it('should show back button', () => {
    const save = fixture.nativeElement.querySelector('#btn_list').textContent;
    expect(save).toBe('Terug');
  });
});
