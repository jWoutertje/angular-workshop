import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ItemService} from '../../services/item.service';
import {Item} from '../../models/item';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {
  title = 'Nieuw item';
  subTitle = 'NewComponent';
  btnSave = 'Opslaan';
  btnGoToList = 'Terug';
  item: Item;

  constructor(
    private router: Router,
    private itemService: ItemService
  ) {
  }

  ngOnInit(): void {
    this.item = new Item();
  }

  save(): void {
    this.itemService
      .postItem(this.item)
      .subscribe(
        item => {
          this.item = item;
          this.router.navigate(['/items', item.id]);
        }
      );
  }

  goToList(): void {
    this.router.navigate(['/all']);
  }
}
