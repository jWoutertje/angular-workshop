import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ItemService} from '../../services/item.service';
import {Item} from '../../models/item';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  title = 'Overzicht van alle items';
  subTitle = 'ListComponent';
  btnGoToNew = 'Nieuw';
  items: Item[];

  constructor(
    private router: Router,
    private itemService: ItemService
  ) {
  }

  ngOnInit(): void {
    this.getItems();
  }

  getItems(): void {
    this.itemService
      .getItems()
      .subscribe(
        items => {
          this.items = items;
        }
      );
  }

  goToDetail(text: number): void {
    this.router.navigate([`/item/${text}`]);
  }

  goToNew(): void {
    this.router.navigate(['/new']);
  }
}
