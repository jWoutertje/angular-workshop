import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ListComponent} from './list.component';
import {ItemService} from '../../services/item.service';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {of} from 'rxjs';

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;

  beforeEach(async(() => {
    const items = [{id: 1, titel: 'test titel 1', tekst: 'test tekst 1'}, {id: 2, titel: 'test titel 2', tekst: 'test tekst 2'}];
    const itemService = jasmine.createSpyObj('ItemService', ['getItems']);
    itemService.getItems.and.returnValue(of(items));

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [ListComponent],
      providers: [{provide: ItemService, useValue: itemService}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show item titels', () => {
    const titel1 = fixture.nativeElement.querySelector('#item_title_1').textContent;
    expect(titel1).toBe('test titel 1');
    const titel2 = fixture.nativeElement.querySelector('#item_title_2').textContent;
    expect(titel2).toBe('test titel 2');
  });

  it('should show new button', () => {
    const save = fixture.nativeElement.querySelector('#btn_new').textContent;
    expect(save).toBe('Nieuw');
  });
});
