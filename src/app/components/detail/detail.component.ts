import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ItemService} from '../../services/item.service';
import {Item} from '../../models/item';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {
  title = 'Details van een item';
  subTitle = 'DetailComponent';
  btnGoToList = 'Terug';
  btnRemove = 'Verwijder';
  item: Item;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private itemService: ItemService
  ) {
  }

  ngOnInit(): void {
    this.getItemById();
  }

  getItemById(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.itemService
      .getItemById(id)
      .subscribe(
        item => {
          this.item = item;
        }
      );
  }

  remove(): void {
    this.itemService
      .deleteItem(this.item.id)
      .subscribe(
        () => {
          this.router.navigate(['/items']);
        }
      );
  }

  goToList(): void {
    this.router.navigate(['/all']);
  }
}
