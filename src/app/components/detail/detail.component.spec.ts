import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DetailComponent } from './detail.component';
import {RouterTestingModule} from '@angular/router/testing';
import {HttpClientModule} from '@angular/common/http';
import {ItemService} from '../../services/item.service';
import {of} from 'rxjs';

describe('DetailComponent', () => {
  let component: DetailComponent;
  let fixture: ComponentFixture<DetailComponent>;

  beforeEach(async(() => {
    const item = {id: 1, titel: 'test titel', tekst: 'test tekst'};
    const itemService = jasmine.createSpyObj('ItemService', ['getItemById']);
    itemService.getItemById.and.returnValue( of(item) );

    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientModule],
      declarations: [ DetailComponent ],
      providers: [{provide: ItemService, useValue: itemService}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show item titel', () => {
    const titel = fixture.nativeElement.querySelector('#item_title').textContent;
    expect(titel).toBe('test titel');
  });

  it('should show item tekst', () => {
    const tekst = fixture.nativeElement.querySelector('#item_text').textContent;
    expect(tekst).toBe('test tekst');
  });

  it('should show remove button', () => {
    const save = fixture.nativeElement.querySelector('#btn_remove').textContent;
    expect(save).toBe('Verwijder');
  });

  it('should show back button', () => {
    const save = fixture.nativeElement.querySelector('#btn_list').textContent;
    expect(save).toBe('Terug');
  });

});
