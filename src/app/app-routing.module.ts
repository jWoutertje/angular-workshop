import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ListComponent} from './components/list/list.component';
import {DetailComponent} from './components/detail/detail.component';
import {NewComponent} from './components/new/new.component';


const routes: Routes = [
  {path: 'all', component: ListComponent},
  {path: 'item/:id', component: DetailComponent},
  {path: 'new', component: NewComponent},
  {path: '', redirectTo: '/all', pathMatch: 'full'},
  {path: '**', redirectTo: '/all'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
