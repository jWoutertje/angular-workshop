import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './components/app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ListComponent} from './components/list/list.component';
import {DetailComponent} from './components/detail/detail.component';

import { HttpClientModule } from '@angular/common/http';
import {NewComponent} from './components/new/new.component';
import {RouterModule} from '@angular/router';
import {ItemService} from './services/item.service';
import {FormsModule} from '@angular/forms';
import {MaterialModule} from './material';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    DetailComponent,
    NewComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [ItemService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
