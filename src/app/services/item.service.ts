import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {throwError as observableThrowError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

import {Item} from '../models/item';
import {environment} from '../../environments/environment';


@Injectable()
export class ItemService {
  private itemUrl = environment.apiUrl + '/items';

  constructor(
    private http: HttpClient
  ) {
  }

  getItems() {
    return this.http
      .get<Item[]>(this.itemUrl)
      .pipe(map(data => data), catchError(this.handleError));
  }

  getItemById(id: number) {
    const url = `${this.itemUrl}/${id}`;
    return this.http
      .get<Item>(url)
      .pipe(map(data => data), catchError(this.handleError));
  }

  postItem(item: Item) {
    return this.http
      .post<Item>(this.itemUrl, item)
      .pipe(catchError(this.handleError));
  }

  deleteItem(id: number) {
    const url = `${this.itemUrl}/${id}`;
    return this.http
      .delete<void>(url)
      .pipe(catchError(this.handleError));
  }

  private handleError(res: HttpErrorResponse | any) {
    console.error(res.error || res.body.error);
    return observableThrowError(res.error || 'Server error');
  }
}
